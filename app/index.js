/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppRoot from './core/ui/layouts/AppRoot';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppRoot);
