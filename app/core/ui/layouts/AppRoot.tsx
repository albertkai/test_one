import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import UserProfile from '../pages/UserProfile';

const AppRoot: () => React.Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <UserProfile />
      </SafeAreaView>
    </>
  );
};

export default AppRoot;
