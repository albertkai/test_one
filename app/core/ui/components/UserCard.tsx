import React from 'react';
import styled from 'styled-components';
import {View} from 'react-native';
import Avatar from '../atoms/Avatar';
import RemoveButton from '../atoms/RemoveButton';
import {CARD_WIDTH, CARD_HEIGHT} from '../../api/constants';
import {deletePhoto} from '../../api/mutations';

const UserCard: (photo: any) => React.Node = ({photo: {id,url}}) => {
  return (
    <Cont>
      <Card>
        <Avatar uri={url} />
        <RemoveButton onPress={() => deletePhoto(id)} />
      </Card>
    </Cont>
  );
};

const Cont = styled(View)`
  padding: 10px;
  width: ${CARD_WIDTH}px;
  height: ${CARD_HEIGHT}px;
`;

const Card = styled(View)`
  position: relative;
  box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.13);
  border-radius: 6px;
  flex: 1;
  align-items: center;
  background-color: red;
  justify-content: center;
`;

export default UserCard;
