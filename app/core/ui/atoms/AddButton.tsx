import React from 'react';
import styled from 'styled-components';
import {TouchableOpacity, Text} from 'react-native';
import {addPhoto} from '../../api/mutations';

const AddButton: (props: any) => React.Node = ({length, ...rest}) => {
  const add = () => {
    addPhoto('https://randomuser.me/api/portraits/women/21.jpg', length + 1);
  };

  return (
    <Root onPress={add} {...rest}>
      <Text>Add photo +</Text>
    </Root>
  );
};

const Root = styled(TouchableOpacity)`
  width: 100%;
  align-items: center;
  margin-top: 18px;
`;

export default AddButton;
