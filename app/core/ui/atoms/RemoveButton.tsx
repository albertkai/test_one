import React from 'react';
import styled from 'styled-components';
import {TouchableOpacity, Text} from 'react-native';

const RemoveButton: (props: any) => React.Node = (props) => {
  return (
    <Root {...props}>
      <Text>x</Text>
    </Root>
  );
};

const Root = styled(TouchableOpacity)`
  position: absolute;
  top: -12px;
  right: -12px;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: rgba(255, 0, 0, 0.7);
  align-items: center;
  justify-content: center;
`;

export default RemoveButton;
