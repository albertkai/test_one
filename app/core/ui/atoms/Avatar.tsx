import React from 'react';
import styled from 'styled-components';
import {Image} from 'react-native';

const Avatar: (props: any) => React.Node = ({ uri }) => {
  return <Root source={{ uri }} resizeMode={'cover'} />;
};

const Root = styled(Image)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 6px;
`;

export default Avatar;
