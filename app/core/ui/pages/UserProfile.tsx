import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import styled from 'styled-components';
import UserCard from '../components/UserCard';
import AddButton from '../atoms/AddButton';
import {DragSortableView} from 'react-native-drag-sort';
import {CARD_WIDTH, CARD_HEIGHT, CONTAINER_WIDTH} from '../../api/constants';
import {getMemberPhotos} from '../../api/queries';

const UserProfile: () => React.Node = () => {
  const [photos, setPhotos] = useState([]);
  useEffect(() => {
    const fetchPhotos = async () => {
      try {
        const photos = await getMemberPhotos();
        setPhotos(photos);
      } catch (e) {
        console.log(e);
      }
    };
    fetchPhotos();
  }, []);

  const keyExtractor = (photo: any) => photo.id;

  const renderItem = (photo: any) => <UserCard photo={photo} />;

  return (
    <Cont>
      <DragSortableView
        dataSource={photos}
        parentWidth={CONTAINER_WIDTH}
        childrenHeight={CARD_HEIGHT}
        childrenWidth={CARD_WIDTH}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
      <AddButton />
    </Cont>
  );
};

const Cont = styled(View)``;

export default UserProfile;
