import {vw} from "./helpers";

export const CARD_WIDTH = 33.33 * vw;
export const CARD_HEIGHT = 53 * vw;
export const CONTAINER_WIDTH = 100 * vw;
