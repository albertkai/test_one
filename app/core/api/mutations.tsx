export const addPhoto = async (url: string, position: number) => {
  try {
    const result = await fetch(
      `http://localhost:3000/photos/${Math.random()
        .toString()
        .replace('.', '')}`,
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          url,
          width: 100,
          height: 100,
          position,
          centerX: 0,
          centerY: 0,
        }),
      },
    );
    console.log('res', result);
  } catch (e) {
    console.log(e);
  }
};

export const deletePhoto = async (id: string) => {
  try {
    const result = await fetch(`http://localhost:3000/photos/${id}`, {
      method: 'DELETE',
    });
    console.log('res', result);
  } catch (e) {
    console.log(e);
  }
};
